#-*- coding: utf-8 -*-
from django.db.models import *
from django.utils.translation import ugettext_lazy as _

class City(Model):
    name = CharField(max_length=1000, blank=False, default=_(u'Gotam'), verbose_name=_(u'city'), db_index=True)
    class Meta:
        verbose_name = _(u'city')
        verbose_name_plural = _(u'cities')

    def __unicode__(self,*args, **kwargs):
        return u'г. %s' % (self.name,)

class Street(Model):
    town = ForeignKey('City', verbose_name=_(u'city'), db_index=True)
    name = CharField(max_length=255, blank=False, verbose_name=(u'street'), db_index=True)

    class Meta:
        ordering = ['name',]
        unique_together = (('name',),)
        verbose_name = _(u'street')
        verbose_name_plural = _(u'streets')

    def __unicode__(self, *args, **kwargs):
        street_shortcut = _(u'st.')
        return "%s %s%s" % (self.town.name, street_shortcut, self.name)

class House(Model):
    street = ForeignKey('Street', verbose_name=_(u'street'), db_index=True)
    num = CharField(max_length=100, blank=False, verbose_name=_(u'house'), db_index=True)
    sub = CharField(max_length=100, blank=True, default='0', verbose_name=_(u'building'), db_index=True)

    class Meta:
        unique_together = (('street', 'num', 'sub',),)
        verbose_name = _(u'house')
        verbose_name_plural = _(u'houses')

    def __unicode__(self,*args, **kwargs):
        house_shortcut = _(u"h.")
        if self.sub == None:
            return unicode("%s %s%s" % (self.street.__unicode__(), house_shortcut, self.num,))
        elif len(self.sub):
            return unicode("%s %s%s/%s" % (self.street.__unicode__(), house_shortcut, self.num, self.sub))
        else:
            return unicode("%s %s%s" % (self.street.__unicode__(), house_shortcut, self.num,))

class Office(Model):
    house = ForeignKey('House', verbose_name=(u'house'), db_index=True)
    name = CharField(max_length=100, blank=False, verbose_name=_(u'name'), db_index=True)

    class Meta:
        unique_together = (('house', 'name',),)
        verbose_name = _(u'office')
        verbose_name_plural = _(u'office')

    def __unicode__(self,*args, **kwargs):
        return unicode(u"%s \"%s\"" % (self.house.__unicode__(), self.name,))

class Unit(Model):
    office = ForeignKey('Office', verbose_name=_(u'office'), db_index=True)
    name = CharField(max_length=100, blank=False, verbose_name=(u'name'), db_index=True)

    class Meta:
        unique_together = (('office', 'name',),)
        verbose_name = _(u'unit')
        verbose_name_plural = _(u'unit')

    def __unicode__(self,*args, **kwargs):
        return unicode(u"%s %s" % (self.office.__unicode__(), self.name,))

